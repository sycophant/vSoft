package com.emp;

public class Employee {

	private String ename, dname;
	private int eno;

	Employee(int eno, String ename, String dname){
		this.eno = eno;
		this.ename = ename;
		this.dname = dname;
	}

	Employee(){
		System.out.println("Employee created!");
	}
    
	public void setEno(int eno){this.eno = eno;}
	public void setEname(String ename){this.ename = ename;}
	public void setDname(String dname){this.dname = dname;}

	public int getEno(){return this.eno;}
	public String getEname(){return this.ename;}
	public String getDname(){return this.dname;}

	public int CalculateSalary(String dname){
		return 0;
	}

}
