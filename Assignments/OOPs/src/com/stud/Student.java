package com.stud;

public class Student {
    
    private String Name, Address, MobileNo, Course;
    private int StudentId;

    Student(){
    	System.out.println("Student created!");
    }

    Student(int StudentId, String Name, String Address, String MobileNo, String Course){
    	this.StudentId = StudentId;
    	this.Name = Name;
    	this.Address = Address;
    	this.MobileNo = MobileNo;
    	this.Course = Course;
    }

    public int getStudentId(){return this.StudentId;}
	public String getName(){return this.Name;}
	public String getAddress(){return this.Address;}
	public String getMobileNo(){return this.MobileNo;}
	public String getCourse(){return this.Course;}

    public void setStudentId(int StudentId){this.StudentId = StudentId;}
	public void setName(String Name){this.Name = Name;}
	public void setAddress(String Address){this.Address = Address;}
	public void setMobileNo(String MobileNo){this.MobileNo = MobileNo;}
	public void setCourse(String Course){this.Course = Course;}

	public int CalculateFee(int StudentId){
		return 0;
	}
}
