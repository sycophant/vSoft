import java.util.Scanner;

public class Assignment3b {

    private static int BILL = 0;
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        Assignment3b acc = new Assignment3b();
        acc.mainMenu();
    }

    static void displayMenu(String[] options){

        String design = "";

        for(int i = 0; i < options.length; i++){
            design += "\n" + ( i + 1 ) + ") " + options[i];
        }

        System.out.println(design);
        System.out.println();
    }

    public void mainMenu(){

        Assignment3b acc = new Assignment3b();
        Birayni b = new Birayni();
        Drinks d = new Drinks();
        IceCream ice = new IceCream();
        String[] optionArr = new String[]{"Birayni", "Drinks", "Ice-Cream", "BILL", "Exit"};
        displayMenu(optionArr);
        System.out.print("Enter your selection: ");
        int selection = sc.nextInt();

        switch (selection){
            case 1:
                b.birayniMenu();
                break;
            case 2:
                d.drinksMenu();
                break;
            case 3:
                ice.iceCreamMenu();
                break;
            case 4:
                acc.displayBILL();
                break;
            case 5:
                System.exit(0);
                break;
            default:
                System.out.println("Invalid input!");
                mainMenu();
        }
    }

    public void getInput(int kind, int priceEach){
        System.out.print("How many order: ");
        int Quantity = sc.nextInt();
        kind += Quantity;
        BILL += Quantity * priceEach;

    }

    public void displayBILL(){
        System.out.println("Total BILL: $" + BILL);
        mainMenu();
    }
}

class Birayni extends Assignment3b{

    private int chickenBnQuantity, chickenBnPrice = 8, vegBnQuantity, vegBnPrice = 6, fishBnQuantity, fishBnPrice = 9;

    public void birayniMenu(){
        String[] optionArr = new String[]{"ChickenBn", "VegBn", "FishBn", "Back"};
        Assignment3b.displayMenu(optionArr);
        System.out.print("Enter your selection: ");
        int selection = Assignment3b.sc.nextInt();

        switch (selection){
            case 1:
            	super.getInput(chickenBnQuantity, chickenBnPrice);
                break;
            case 2:
            	super.getInput(vegBnQuantity, vegBnPrice);
                break;
            case 3:
            	super.getInput(fishBnQuantity, fishBnPrice);
                break;
            case 4:
            	super.mainMenu();
                break;
            default:
                System.out.println("Invalid input!");

        }
        birayniMenu();
    }
}

class Drinks extends Assignment3b{

    public void drinksMenu(){
        SoftDrinks soft = new SoftDrinks();
        HotDrinks hot = new HotDrinks();
        String[] optionArr = new String[]{"Soft Drinks", "Hot Drinks", "Back"};
        Assignment3b.displayMenu(optionArr);
        System.out.print("Enter your selection: ");
        int selection = Assignment3b.sc.nextInt();

        switch (selection){
            case 1:
            	soft.softDrinksMenu();
                break;
            case 2:
            	hot.hotDrinksMenu();
                break;
            case 3:
            	super.mainMenu();
                break;
            default:
                System.out.println("Invalid input!");

        }
        drinksMenu();
    }
}

class SoftDrinks extends Drinks{

    private int spriteQuantity, spritePrice = 3, thumsUpQuantity, thumsUpPrice = 4;

    public void softDrinksMenu(){
        String[] optionArr = new String[]{"Sprite", "Thums Up", "Back"};
        Assignment3b.displayMenu(optionArr);
        System.out.print("Enter your selection: ");
        int selection = Assignment3b.sc.nextInt();

        switch (selection){
            case 1:
            	super.getInput(spriteQuantity, spritePrice);
                break;
            case 2:
            	super.getInput(thumsUpQuantity, thumsUpPrice);
                break;
            case 3:
            	super.drinksMenu();
                break;
            default:
                System.out.println("Invalid input!");
                softDrinksMenu();
        }
    }
}

class HotDrinks extends Drinks{

    private int teaQuantity, teaPrice = 3, coffeeQuantity, coffeePrice = 5;

    public void hotDrinksMenu(){
        String[] optionArr = new String[]{"Tea", "Coffee", "Back"};
        Assignment3b.displayMenu(optionArr);
        System.out.print("Enter your selection: ");
        int selection = Assignment3b.sc.nextInt();

        switch (selection){
            case 1:
            	super.getInput(teaQuantity, teaPrice);
                break;
            case 2:
            	super.getInput(coffeeQuantity, coffeePrice);
                break;
            case 3:
            	super.drinksMenu();
                break;
            default:
                System.out.println("Invalid input!");
                hotDrinksMenu();
        }
    }
}

class IceCream extends Assignment3b{

    private int butterQuantity, butterPrice = 6, vanilaQuantity, vanilaPrice = 7;

    public void iceCreamMenu(){
        String[] optionArr = new String[]{"Butter Ice-Cream", "Vanila Ice-Cream", "Back"};
        Assignment3b.displayMenu(optionArr);
        System.out.print("Enter your selection: ");
        int selection = Assignment3b.sc.nextInt();

        switch (selection){
            case 1:
            	super.getInput(butterQuantity, butterPrice);
                break;
            case 2:
            	super.getInput(vanilaQuantity, vanilaPrice);
                break;
            case 3:
            	super.mainMenu();
                break;
            default:
                System.out.println("Invalid input!");

        }
        iceCreamMenu();
    }
}