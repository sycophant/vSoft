package com.HiberTest.pojo;

import javax.annotation.processing.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Student.class)
public abstract class Student_ {

	public static volatile SingularAttribute<Student, Integer> sno;
	public static volatile SingularAttribute<Student, String> sname;
	public static volatile SingularAttribute<Student, String> saddr;

	public static final String SNO = "sno";
	public static final String SNAME = "sname";
	public static final String SADDR = "saddr";

}

