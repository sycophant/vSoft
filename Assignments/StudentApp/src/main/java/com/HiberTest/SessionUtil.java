package com.HiberTest;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class SessionUtil {

    private static final String
        HBR_CONF = "com\\HiberTest\\hibernate.cfg.xml";

	private static SessionFactory factory = null;

	static{
		factory = new Configuration().configure(HBR_CONF).buildSessionFactory();
	}

	public static Session getSession(){
		return factory.openSession();
	}

	public void closeConn(Session sess){
		if (sess != null) sess.close();
	}
}
