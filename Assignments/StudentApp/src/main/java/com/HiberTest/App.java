package com.HiberTest;

import com.HiberTest.pojo.Student;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class App{

    private static final String
            HBR_CONF = "com\\HiberTest\\hibernate.cfg.xml";

    public static void main( String[] args )
    {

        SessionFactory sessF 
            = new Configuration().configure(HBR_CONF).buildSessionFactory();

        Session sess = sessF.openSession();
        
        Transaction tx = sess.beginTransaction();

        Student student = new Student();

        student.setSno(5);
        student.setSaddr("Hi");
        student.setSname("Hello");

        sess.saveOrUpdate(student);
        tx.commit();

    }
}
