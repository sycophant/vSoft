package com.EmployeeBulk;

import java.util.List;

import com.EmployeeBulk.pojo.Employee;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.NativeQuery;

public class NativeSQLClient {
    
    public static void main(String[] args){
    	Session session = SessionUtility.getSession();
        Transaction tx = session.beginTransaction();

        NativeQuery sqlQuery = session.createNativeQuery("select * from employee");
        sqlQuery.addEntity(Employee.class);

        List<Employee> empList = sqlQuery.getResultList();
    }
}