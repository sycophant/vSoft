package com.EmployeeBulk;

import java.util.List;

import javax.persistence.Query;

import com.EmployeeBulk.pojo.Employee;

import org.hibernate.Session;
import org.hibernate.Transaction;

public class HQLClient {

	public static void main(String[] args){

	    final Session session = SessionUtility.getSession();
	    final Transaction tx = session.beginTransaction();

		Query q = session.createQuery("select firstName FROM Employee where eno<:enumb");
	    q.setParameter("enumb", 100);

	    List<Employee> emplList = q.getResultList();
	    for (Employee obj : emplList){
	    	System.out.println(obj.getEno());
	    }
	}
}
