package com.EmployeeBulk;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class SessionUtility {
    
    private static SessionFactory factory = null;
    private static final String
        HBR_CONF = "com\\EmployeeBulk\\pojo\\hibernate.cfg.xml";

    static{
        factory = new Configuration().configure(HBR_CONF).buildSessionFactory();
    }

    public static Session getSession(){
        return factory.openSession();
    }

    public void closeConn(Session session){
        if(session != null) session.close();
    }
}
