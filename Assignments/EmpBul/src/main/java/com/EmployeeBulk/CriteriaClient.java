package com.EmployeeBulk;

import java.util.List;

import com.EmployeeBulk.pojo.Employee;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

public class CriteriaClient {
    public static void main(String[] args){
    	Session session = SessionUtility.getSession();
    	Transaction tx = session.beginTransaction();

    	Criteria cQuery = session.createCriteria(Employee.class);
        cQuery.add(Restrictions.gt("eno", 100));
    	List<Employee> lst = cQuery.list();

    	for (Employee i : lst){
    		System.out.println(i.getEno());
    	}

    }
}
