public class SwapDemo {

	// private value init
	private int fNo = 1, sNo = 2, tNo = 3, ffNo = 4, jioCost = 699, discPct = 18, salary = 85000, tax = 20;
	private double pizzaCost = 399.0, deposit = 5E5, interest = 6.8;

	// getters
	public int getFno() {return fNo;}
	public int getSno() {return sNo;}
	public int getTno() {return tNo;}
	public int getFfno() {return ffNo;}
	public int getJioCost() {return jioCost;}
	public int getDiscPct() {return discPct;}
	public double getPizzaCost() {return pizzaCost;}
	public int getSalary() {return salary;}
	public int getTax() {return tax;}
	public double getBank() {return deposit;}
	public double getInterest() {return interest;}

	// swap two numbers
	public void Swap2Demo(){

		int first = this.getFno(),
			second = this.getSno();

		System.out.println(first + ": first " + second + ": second");

		int temp = second;
		second = first;
		first = temp;

		System.out.println(first + ": first " + second + ": second " + temp + ": temp");

		second = first + second;
		first = second - first;
		second = second - first;

		System.out.println(first + ": first " + second + ": second" + " no temp");
	}

	public void Swap3Demo(){

		int first = this.getFno(),
			second = this.getSno(),
			third = this.getTno();

		System.out.println(first + ": first " + second + ": second " + third + ": third");

		int temp = third;
		third = first;
		first = temp;
		temp = second;
		second = first;
		first = temp;

		System.out.println(first + ": first " + second + ": second " + third + ": third " + temp + ": temp");

		third = first + third;
		first = third - first;
		third = third - first;
		second = first + second;
		first = second - first;
		second = second - first;

		System.out.println(first + ": first " + second + ": second " + third + ": third" + " no temp");
	}

	public void Swap4Demo(){

		int first = this.getFno(),
			second = this.getSno(),
			third = this.getTno(),
			fourth = this.getFfno();

		System.out.println(first + ": first " + second + ": second " + third + ": third " + fourth + ": fourth");

		int temp = third;
		third = first;
		first = temp;
		temp = second;
		second = first;
		first = temp;
		temp = fourth;
		fourth = second;
		second = temp;

		System.out.println(first + ": first " + second + ": second " + third + ": third " + fourth + ": fourth " + temp + ": temp");

		third = first + third;
		first = third - first;
		third = third - first;
		second = first + second;
		first = second - first;
		second = second - first;

		System.out.println(first + ": first " + second + ": second " + third + ": third " + fourth + ": fourth" + " no temp");
	}

	public void jioFibre(){
		int cost = this.getJioCost(), pct = this.getDiscPct();
		int finalCost = cost + ((cost * pct) / 100);
		System.out.println( "jioFibre initial: " + cost + " at " + pct + " percent");
		System.out.println("jioFibre final cost " + finalCost);
	}

	public void pizzaBill(){
		double cost = this.getPizzaCost();
		int pct = this.getDiscPct();
		double finalCost = (cost * 2) + (cost * pct / 100);
		System.out.println("pizza bill: " + Math.round(finalCost * 100.00) / 100.00);
	}

	public void taxAmount(){
		int salary = this.getSalary();
		int tax = this.getTax();
		int finalCost = (salary * tax / 100);
		System.out.println("Tax on Salary: " + finalCost);
	}

	public void bankAccount(){

		double bank = 0.0, interest = this.getInterest(), interestAmount = 0.0;

		for (int i = 0; i < 5; i++){
			bank += this.getBank();
			double yearlyInterest = bank * interest / 100;
			bank -= yearlyInterest;
			interestAmount += yearlyInterest;
		}

		System.out.println("There is " + Math.round(bank * 100.00) / 100.00 + "$ in the bank and interest accrued up to " + Math.round(interestAmount * 100.00) / 100.00 + "$" );
	}

	public static void main(String[] args) {

		SwapDemo swapper = new SwapDemo();

		swapper.Swap2Demo();
		swapper.Swap3Demo();
		swapper.Swap4Demo();
		swapper.jioFibre();
		swapper.pizzaBill();
		swapper.taxAmount();
		swapper.bankAccount();
		System.out.println();
	}
}