package pojo;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="product")
public class Product {

    public Product(int id, String name, int qty, double price) {
        this.id = id;
        this.name = name;
        this.qty = qty;
        this.price = price;
    }
    public Product() {
        System.out.println("Product Created!!");
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getQty() {
        return qty;
    }
    public void setQty(int qty) {
        this.qty = qty;
    }
    public double getPrice() {
        return price;
    }
    public void setPrice(double price) {
        this.price = price;
    }

    @GenericGenerator (name = "myGen", strategy = "increment")
    @GeneratedValue(generator = "myGen", strategy = GenerationType.AUTO) @Id
    private int id;
    private String name;
    private int qty;
    private double price;
}
