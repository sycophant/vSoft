package ProductDAO;

import java.util.List;

import pojo.Product;

public interface ProductDAO {

    void addProduct(Product product);
    List<Product> viewAllProducts();
    Product viewProduct(int id);
    boolean deleteProduct(int id);
}
