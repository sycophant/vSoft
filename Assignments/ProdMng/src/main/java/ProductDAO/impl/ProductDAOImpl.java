package ProductDAO.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import ProductDAO.ProductDAO;
import Utility.SessionUtility;
import pojo.Product;

public class ProductDAOImpl implements ProductDAO {

    private Session ses = SessionUtility.getSession();
    private Transaction tx;

    public ProductDAOImpl() {

    }

    public void addProduct(Product product) {
        ses = SessionUtility.getSession();
        ses.save(product);
        ses.close();

    }

    public List<Product> viewAllProducts() {
        ses = SessionUtility.getSession();
        Query query = ses.createQuery("from product");
        List<Product> products = query.getResultList();
        ses.close();
        return products;
    }

    public Product viewProduct(int id) {
        ses = SessionUtility.getSession();
        Query query = ses.createQuery("from product where id=:pId");
        query.setParameter("pId",id);
        Product product = (Product) query.uniqueResult();
        ses.close();
        return product;
    }

    public boolean deleteProduct(int id) {
        ses = SessionUtility.getSession();
        tx = ses.beginTransaction();
        Query query = ses.createQuery("delete from product where id=:pId");
        query.setParameter("pId", id);
        int count = query.executeUpdate();
        tx.commit();
        if(count > 0){
            return true;
        }
        return false;
    }
}
