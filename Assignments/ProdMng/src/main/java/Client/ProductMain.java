package Client;

import java.util.List;
import java.util.Scanner;

import ProductDAO.impl.ProductDAOImpl;
import pojo.Product;

public class ProductMain {

	public static void main(String[] args) {
        MainMenu();
    }

    //Custom input
    static int toInt(String mes){
        System.out.print(mes);
        return sc.nextInt();
    }
    static Double toDouble(String mes){
        System.out.print(mes);
        return sc.nextDouble();
    }
    static String toString(String mes){
        System.out.print(mes);
        return sc.next();
    }

    static void MainMenu(){
        System.out.println(GAP);
        System.out.println(PROD_MENU);
        System.out.println(GAP);

        int selection = toInt(ACT_PROMPT);
        switch (selection){
            case 1:
                ProductMenu();
                break;
            case 2:
                System.exit(0);
        }
        MainMenu();
    }

    static void ProductMenu(){
        System.out.println(GAP);
        System.out.println(PROD_MENU2);
        System.out.println(GAP);

        int selection = toInt(ACT_PROMPT);
        switch (selection){
            case  1:
                AddProduct();
                break;
            case  2:
                ViewAllProduct();
                break;
            case  3:
                SearchProduct();
                break;
            case  4:
                DeleteProduct();
                break;
            case  5:
                MainMenu();
                break;
        }
        ProductMenu();
    }

    static void AddProduct(){
        String name = toString(PRODNAME);
        int qty = toInt(PRODQTY);
        double price = toDouble(PRODPRICE);
        productDAO.addProduct(new Product(0, name, qty, price));
    }

    static void ViewAllProduct(){
        List<Product> list = productDAO.viewAllProducts();
        if(list.size() > 0){
            list.forEach(item -> System.out.println(item));
        }else {
            System.out.println(PROD_UNFOUND);
        }
    }

    static void SearchProduct(){
        int id = toInt(PRODID);
        Product product = productDAO.viewProduct(id);
        if(product != null){
            System.out.println(product);
        }else {
            System.out.println(PROD_UNFOUND);
        }
    }

    static void DeleteProduct(){
        int id = toInt(PRODID);
        boolean success = productDAO.deleteProduct(id);
        if(success == true){
            System.out.println(PROD_DEL);
        }else {
            System.out.println(PROD_UNFOUND);
        }
    }

    static ProductDAOImpl productDAO = new ProductDAOImpl();
    static Scanner sc = new Scanner(System.in);

    private static final String
        GAP = "--------------",
        PROD_MENU = "1) Product\n2) Exit",
        PROD_MENU2 = "1) Add\n2) View All\n3) Search\n4) Delete \n5) Back",
        PROD_UNFOUND = "Product not available",
        PROD_DEL = "Delete product success",
        ACT_PROMPT = "Enter your action: ",
        PRODID = "Enter product id: ",
        PRODNAME = "Enter Product Name: ",
        PRODPRICE = "Enter Price: ",
        PRODQTY = "Enter Quantity: ";
}
