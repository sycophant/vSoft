import java.util.Scanner;

public class Assignment2{

	public boolean primeTest(int x){
        if (x <= 1) return false;
  
        for (int i = 2; i < x; i++){
	        if (x % i == 0) return false;
        }
        return true;
    }

    public void printEvenSeq(){
    	for (int i = 1; i < 100; i++){
    		if (i%2 != 0) System.out.print(i + " ");
    	}
    }

    public void printPrimeSeq(){
    	for (int i = 1; i < 100; i++){
    		if (this.primeTest(i)) System.out.print(i + " ");
    	}
    }

    public int printFactorial(int x){
    	if (x < 2) {return x;}
    	else return x * printFactorial(x - 1);
    }

    public int printFib(int x){
    	if (x < 2) {return x;}
    	else return printFib(x - 1) + printFib(x - 2);
    }

	public boolean isPalindrome(String s) {

		s = s.replaceAll("[\\W]", "");

		for (int i = 0; i < (s.length()/2); ++i) {
			if (s.charAt(i) != s.charAt(s.length() - i - 1)) {
				return false;
			}
		}
		return true;
	}

	public boolean isArmstrong(int x){

		String temp = Integer.toString(x);
		int[] newGuess = new int[temp.length()];
		int armStrong = 0;

		for (int i = 0; i < temp.length(); i++){
		    newGuess[i] = temp.charAt(i) - '0';
		}

		for (int i = 0; i < newGuess.length; i++){
			armStrong += Math.pow(newGuess[i], newGuess.length);
		}

		if (x == armStrong) {return true;}
		else return false;
	}

	public static void main(String[] args) {
		Assignment2 c = new Assignment2();
		Scanner sc = new Scanner(System.in);
		int select = sc.nextInt();
		switch (select){
			case 1:
				int x = sc.nextInt();
				System.out.println(c.primeTest(x));
				break;
			case 2:
				c.printEvenSeq();
				break;
			case 3:
				c.printPrimeSeq();
				break;
			case 4:
				int y = sc.nextInt();
				System.out.println(c.printFib(y));
				break;
			case 5:
				String z = sc.nextLine();
				System.out.print(c.isPalindrome(z));
				break;
			case 6:
				int l = sc.nextInt();
				System.out.print(c.printFactorial(l));
				break;
			case 7:
				int m = sc.nextInt();
				System.out.print(c.isArmstrong(m));
				break;
			default:
				sc.close();
				break;
		}
	}
}