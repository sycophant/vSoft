package com.client;

import java.util.Scanner;

import com.dao.impl.EmployeeDAOImpl;
import com.dao.impl.StudentDAOImpl;

public class StuEmpCli {

    public static final boolean StuImplCLI(){
        do {
            StudentDAOImpl stuImpl = new StudentDAOImpl();
            int options = scn.nextInt();

            switch(options){
            case 1:
                int sno = scn.nextInt();
                stuImpl.getStudent(sno);
                break;
            case 2:
                stuImpl.getAllStudents();
                break;
            case 3:
                stuImpl.makeStudent();
                break;
            case 4:
                int sno2 = scn.nextInt();
                stuImpl.updateStudent(sno2);
                break;
            case 5:
                System.out.println(ERROR_MSG);
                return false;
            default:
                break;
            }
        } while (true);
    }

    public static final boolean EmpImplCLI(){
        do {
            EmployeeDAOImpl emImpl = new EmployeeDAOImpl();
            int options = scn.nextInt();

            switch(options){
            case 1:
                int eno = scn.nextInt();
                emImpl.getEmployee(eno);
                break;
            case 2:
                emImpl.getAllEmployees();
                break;
            case 3:
                emImpl.makeEmployee();
                break;
            case 4:
                int eno2 = scn.nextInt();
                emImpl.updateEmployee(eno2);
                break;
            case 5:
                System.out.println(ERROR_MSG);
                return false;
            default:
                break;
            }
        } while (true);
    }

    public static void main(String[] args) {

    	while(true){
    		System.out.println(WELCOME_MSG + PORTAL_DETS);
    		System.out.print(">>\t");

    		int options = scn.nextInt();
    		switch(options){
    			case 1:
                    StuImplCLI();
    				break;
    			case 2:
                    EmpImplCLI();
    				break;
    			case 3:
    				System.exit(0);
                    break;
    			default:
    				System.out.println(ERROR_MSG);
    		}
    	}
    }

    public static final Scanner scn = new Scanner(System.in);

    public static final String
        PORTAL_DETS = "\n 1 | Student" + "\n 2 | Employee" + "\n 3 | Exit",
        WELCOME_MSG = "\n Welcome to the Student and Employee System!! \n",
        ERROR_MSG = "\n Please input a valid option! \n";

}
