package com.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

import com.dao.StudentDAO;
import com.pojo.Address;
import com.pojo.Student;

public class StudentDAOImpl implements StudentDAO {

    private ArrayList<Student> stuList = new ArrayList<Student>();
    private ArrayList<Address> addList = new ArrayList<Address>();
    public Scanner scan = new Scanner(System.in);

    public Student stuMake(){

        System.out.println("Give Student Dets");
        int sno = scan.nextInt();
        String sname = scan.next();
        double courseFee = scan.nextInt();

        Address addr = addMake();
        addList.add(addr);

        return new Student(sno, sname, courseFee, addr);
    }

    public Address addMake(){

        System.out.println("Give Address Dets");
        String HouseNumber = scan.next();
        String StreetNumber = scan.next();
        String City = scan.next();

        return new Address(HouseNumber, StreetNumber, City);
    }

    public void getStudent(final int sno){
        List<Student> result = stuList.stream()
                    .filter(stu->stu.getSno() == sno)
                    .collect(Collectors.toList());
        
        result.forEach(x->System.out.println(x.toString()));
    }

    public String getAllStudents(){
        return stuList.toString();
    }

    public void makeStudent(){
        Student stu = stuMake();
        stuList.add(stu);
        System.out.println(stu.toString());
    }
    
    public void updateStudent(final int sno){
        Student stu = stuMake();
        stuList.set(sno, stu);
    }
}
