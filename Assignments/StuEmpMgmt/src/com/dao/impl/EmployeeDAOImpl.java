package com.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

import com.dao.EmployeeDAO;
import com.pojo.Address;
import com.pojo.Employee;

public class EmployeeDAOImpl implements EmployeeDAO{

	private ArrayList<Employee> emList = new ArrayList<Employee>();
	private ArrayList<Address> addList = new ArrayList<Address>();
	public Scanner scan = new Scanner(System.in);

	public Employee empMake(){

		System.out.println("Give Employee Dets");
		int eno = scan.nextInt();
		String ename = scan.next();
		String desig = scan.next();
		double salary = scan.nextInt();

		Address addr = addMake();
		addList.add(addr);

		return new Employee(eno, ename, desig, salary, addr);
	}

	public Address addMake(){

		System.out.println("Give Address Dets");
		String HouseNumber = scan.next();
		String StreetNumber = scan.next();
		String City = scan.next();

		return new Address(HouseNumber, StreetNumber, City);
	}

	public void getEmployee(final int eno){
		List<Employee> result = emList.stream()
					.filter(emp->emp.getEno() == eno)
					.collect(Collectors.toList());
		
		result.forEach(x->System.out.println(x.toString()));
	}

    public String getAllEmployees(){
    	return emList.toString();
    }

    public void makeEmployee(){
    	Employee emp = empMake();
    	emList.add(emp);
    	System.out.println(emp.toString());
    }
    
    public void updateEmployee(int eno){
    	Employee emp = empMake();
    	emList.set(eno, emp);
    }
}
