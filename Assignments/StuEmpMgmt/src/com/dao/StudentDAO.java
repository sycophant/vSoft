package com.dao;

public interface StudentDAO {
    void getStudent(int eno);
    String getAllStudents();
    void makeStudent();
    void updateStudent(int eno);
}
