package com.dao;

public interface EmployeeDAO {
    void getEmployee(int eno);
    String getAllEmployees();
    void makeEmployee();
    void updateEmployee(int eno);
}
