package com.pojo;

import javax.persistence.*;

@Entity @Table(name = "Employee")
public class Employee {

	public Employee(
		int eno, String ename, String designation,double salary,
		Address addr){
		this.eno = eno;
		this.ename = ename;
		this.designation = designation;
		this.salary = salary;
		this.addr = addr;
	}

	public Employee(){
		System.out.println("New Employee!!");
	}

	@Override
	public String toString() {
	    String REPR =
	    	this.eno + "\t" +
	    	this.ename + "\t" +
	    	this.designation + "\t" +
	    	this.salary + "\t" +
	    	this.addr;

	    return REPR;
	}
	public int getEno() {
		return eno;
	}
	public void setEno(int eno) {
		this.eno = eno;
	}
	public String getEname() {
		return ename;
	}
	public void setEname(String ename) {
		this.ename = ename;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	public Address getAddress() {
		return addr;
	}

	public void setAddress(Address addr) {
		this.addr = addr;
	}

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int eno;
    private String ename, designation;
    private double salary;
    @Embedded @AttributeOverrides({
    	@AttributeOverride
    		(name = "HouseNumber", column = @Column(name = "House_Number")),
    	@AttributeOverride
    		(name = "StreetNumber", column = @Column(name = "Street_Number"))
    })
    private Address addr;
}
