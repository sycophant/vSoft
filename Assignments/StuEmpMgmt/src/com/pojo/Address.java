package com.pojo;

import javax.persistence.Embeddable;

@Embeddable
public class Address {
    
	public Address(String HouseNumber, String StreetNumber, String City){
		this.HouseNumber = HouseNumber;
		this.StreetNumber = StreetNumber;
		this.City = City;
	}

	public Address(){
		System.out.println("New Address!!");
	}

	@Override public String toString() {
	    String REPR =
	    	this.HouseNumber + "\t" +
			this.StreetNumber + "\t" +
			this.City;

		return REPR;
	}

	public String getHouseNumber() {
		return HouseNumber;
	}

	public void setHouseNumber(String houseNumber) {
		HouseNumber = houseNumber;
	}

	public String getStreetNumber() {
		return StreetNumber;
	}

	public void setStreetNumber(String streetNumber) {
		StreetNumber = streetNumber;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	private String HouseNumber, StreetNumber, City;
}
