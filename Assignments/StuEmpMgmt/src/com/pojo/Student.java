package com.pojo;

import javax.persistence.*;

@Entity @Table(name = "Student")
public class Student {

    public Student(
        int sno, String sname, double courseFee, Address addr){
        this.sno = sno;
        this.sname = sname;
        this.courseFee = courseFee;
        this.addr = addr;
    }

    public Student(){
        System.out.println("New Student!!");
    }

    @Override public String toString() {
        String REPR =
            this.sno + "\t" +
            this.sname + "\t" +
            this.courseFee + "\t" +
            this.addr;

        return REPR;
    }

	public int getSno() {
		return sno;
	}
	public void setSno(int sno) {
		this.sno = sno;
	}
	public String getSname() {
		return sname;
	}
	public void setSname(String sname) {
		this.sname = sname;
	}
	public double getCourseFee() {
		return courseFee;
	}
	public void setCourseFee(double courseFee) {
		this.courseFee = courseFee;
	}
	public Address getAddr() {
		return addr;
	}
	public void setAddr(Address addr) {
		this.addr = addr;
	}

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int sno;
    private String sname;
    private double courseFee;
    @Embedded @AttributeOverrides({
    	@AttributeOverride
    		(name = "HouseNumber", column = @Column(name = "House_Number")),
    	@AttributeOverride
    		(name = "StreetNumber", column = @Column(name = "Street_Number"))
    })
    private Address addr;
}
