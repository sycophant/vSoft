package com.string;
import java.util.*;

public class basics {

	public static Scanner sc = new Scanner(System.in).useDelimiter("\n");;

	public static void Banner(){
		String intro = "Welcome to string methods!",
			options =
			"1) remove all the white spaces from a string \n" + 
			"2) replace lower-case characters with upper-case and vice versa \n" +
			"3) replace the spaces of a string with a specific character \n" + 
			"4) Count the Total Number of Characters in a String \n" + 
			"5) Determine Whether a Given String is Palindrome \n" + 
			"6) Find Maximum and Minimum Occurring Character in a String \n" + 
			"7) Find Reverse of a String \n" + 
			"8) Find the Duplicate Characters in a String \n" + 
			"9) Find the Duplicate Words in a String \n" + 
			"10) Find the Frequency of Characters \n" + 
			"11) Find the Largest and Smallest Word in a String \n" + 
			"12) Swap two String Variables Without Using Third or Temp Variable \n" +
			"13) get the chara at the given index within the String \n" + 
			"14) replace all the 'd' characters with 'f' \n" + 
			"15) find the second most frequent character in a given string \n" + 
			"16) print after removing duplicates from a given string \n" +
			"17) Quit this program.",
			outro = "Choose your operation:",
			gap = "***************************************";

		System.out.println();
		System.out.println(intro);
		System.out.println(gap);
		System.out.println(options);
		System.out.println(gap);
		System.out.println(outro);
	}
    
    public static void main(String[] args) {

    	while(true){
	    	Banner();

	    	int option = sc.nextInt();
	    	String inputStr = sc.next(), words[] = inputStr.split(" ");
	    	char arr[] = inputStr.toCharArray();
	    	int[] freqArr = new int[122];

	    	for(char i : arr) ++freqArr[(int)i];

    		switch(option){  				
    			case 1:
    				System.out.println(inputStr.replaceAll("\\s",""));

    				break;
    			case 2:

    				for (char i : arr){
    					if (Character.isUpperCase(i)) System.out.print(Character.toLowerCase(i));
    					if (Character.isLowerCase(i)) System.out.print(Character.toUpperCase(i));
    				}

    				break;
     			case 3:

     				System.out.println("Character to replace with: ");
     				String temp = sc.next();
     				System.out.println(inputStr.replaceAll("\\s", temp));

    				break;
     			case 4:

     				int counter = 0;
     				for (char i : arr) counter++;
     				System.out.println(counter + " characters in this string.");

    				break;
     			case 5:

     				for (int i = 0; i < arr.length / 2; i++){
     					if (arr[i] != arr[arr.length - i - 1]){
     						System.out.println("Not a palindrome!");
     						break;
     					}else if (i == arr.length / 2 - 1){
     						System.out.println("This is a palindrome.");
     					}
     				}

    				break;
     			case 6:

     				int max = 0, min = 1, maxIndex = 0, minIndex = 0;

     				for (int i = 0; i < freqArr.length; i++){
     					if (max < freqArr[i]){
     						max = freqArr[i];
     						maxIndex = i;
     					}
     					if (freqArr[i] > 0 && min > freqArr[i]){
     						min = freqArr[i];
     						minIndex = i;
     					}
     				}

     				System.out.println("Most recurring character is " + (char)maxIndex +
     					" and least recurring character is " + (char)minIndex);

    				break;
     			case 7:

     				for (int i = arr.length - 1; i >= 0; i--){
     					System.out.print(arr[i]);
     				}

    				break;
     			case 8:

     				for (int i = 0; i < freqArr.length; i++){
     					if (freqArr[i] > 1){
     						System.out.println((char)i + " occurs " + freqArr[i] + " times.");
     					}
     				}

    				break;
     			case 9:

     				List<String> duplicateWords = new ArrayList<String>();

     				for (String i : words){
     					for (String j : words){
     						if (i.equals(j)) duplicateWords.add(i);
     					}
     				}

     				System.out.println("Duplicate words are: " + duplicateWords.toString());

    				break;
     			case 10:

     				for (int i = 0; i < freqArr.length; i++){
     					if (freqArr[i] > 0){
     						System.out.println((char)i + " occurs " + freqArr[i] + " times.");
     					}
     				}

    				break;
     			case 11:

					List<String> strings = Arrays.asList(inputStr);    
					String longest = strings.stream().
						max(Comparator.comparingInt(String::length)).get(),
						shortest = strings.stream().
						min(Comparator.comparingInt(String::length)).get();

					System.out.println(" Longest word is " + longest +
						" and shortest word is " + shortest);

    				break;
     			case 12:

     				System.out.print("Type out the second string: ");

     				String str1 = new String(inputStr), str2 = sc.next();

     				System.out.println("Before swap: " + str1 + "\t" + str2);

     				str1 = str1 + str2;
     				str2 = str1.substring(0, str1.length() - str2.length());
     				str1 = str1.substring(str2.length());

     				System.out.println("After swap: " + str1 + "\t" + str2);

    				break;
     			case 13:

     				System.out.println("Index to get: ");
     				int temp2 = sc.nextInt();

     				System.out.println(arr[temp2] + " is located at that index.");

    				break;
     			case 14:

     				System.out.println(inputStr.replaceAll("d","f"));

    				break;
     			case 15:

     				char max2 = 0, max3 = 0;

					for (int i = 0; i < arr.length; i++){

						if (arr[i] > max2){
							max3 = max2;
							max2 = arr[i];
						}

						else if (arr[i] > max3) max3 = arr[i];
					}

					System.out.println("Second most frequent character is " + max3);

    				break;
     			case 16:

     				List<String> duplicateWords2 = new ArrayList<String>();

     				for (String i : words){
     					for (String j : words){
     						if (i.equals(j)) duplicateWords2.add(i);
     					}
     				}

     				String[] newWords = new String[words.length];

     				duplicateWords2.forEach((e) -> {
     					boolean inArray = false;

     					for (int i = 0; i < words.length; i++){
     						if (e.equals(words[i])) inArray = true;
     					}

     					if (inArray == false) System.out.println(e + " ");
     				});

    				break;
    			case 17:

    				System.exit(0);

    				break;  
     			default:
    				String noInput = "This did nothing!";
    				System.out.println(noInput);
    		}
    	}
    }
}
