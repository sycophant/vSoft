import java.util.Scanner;

public class Assignment3 {

    static Scanner sc = new Scanner(System.in);
    private int bondaQty = 0, priceBonda = 5, quantityDosa = 0,
        priceDosa = 6, quantityIdly = 0, priceIdly = 7, bill = 0;

    public static void main(String[] args) {
        Assignment3 ass = new Assignment3();
        ass.mainMenu();
    }

    public void mainMenu(){

        String[] menuItem = new String[]{"Bonda", "Dosa", "Idly", "Bill", "Exit"};

        displayMenu(menuItem);
        System.out.println("Welcome to the restaurant! The following options are available:");
        System.out.print("Enter your selection: ");

        switch (sc.nextInt()){
            case 1:
                getInput(this.bondaQty, this.priceBonda);
                break;
            case 2:
                getInput(this.quantityDosa, this.priceDosa);
                break;
            case 3:
                getInput(this.quantityIdly, this.priceIdly);
                break;
            case 4:
                displayBill();
                break;
            case 5:
                System.exit(0);
                break;
            default:
                System.out.println("Invalid input!");
                mainMenu();
        }
        mainMenu();
    }

    public void displayMenu(String[] options){

        String design = "";

        for(int i = 0; i < options.length; i++){
            design += "\n" + ( i + 1 ) + ") " + options[i];
        }

        System.out.println(design);
        System.out.println();
    }

    public void getInput(int kind, int priceEach){
        System.out.print(" Choose your option! ");
        int qty = sc.nextInt();
        kind += qty;
        this.bill += qty * priceEach;
    }

    public void displayBill(){
        System.out.println("Your bill amounts to $" + bill + " dollars.");
        mainMenu();
    }
}