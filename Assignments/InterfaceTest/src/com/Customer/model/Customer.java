package com.Customer.model;

public class Customer {
	int cno;
    String cname, cadd, cemail;

    Customer(){
    	System.out.println("Customer created!");
    }

    Customer(int cno, String cname, String cadd, String cemail){
    	this.cno = cno;
    	this.cname = cname;
    	this.cadd = cadd;
    	this.cemail = cemail;
    }

    public void setCno(int cno){this.cno = cno;}
    public void setCname(String cname){this.cname = cname;}
    public void setCadd(String cadd){this.cadd = cadd;}
    public void setCemail(String cemail){this.cemail = cemail;}

    public int getCno(){return this.cno;}
    public String getCname(){return this.cname;}
    public String getCadd(){return this.cadd;}
    public String getCemail(){return this.cemail;}

}
