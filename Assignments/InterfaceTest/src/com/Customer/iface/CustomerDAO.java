package com.Customer.iface;
import com.Customer.model.Customer;

public interface CustomerDAO {
    void addCustomers();
    Customer[] viewAllCustomers();
    Customer viewCustomer();
}
