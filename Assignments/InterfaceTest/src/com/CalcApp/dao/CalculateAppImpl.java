package com.CalcApp.dao;
import com.CalcApp.iface.CalcApp;

public class CalculateAppImpl implements CalcApp{

    @Override public int add(int a, int b){
    	return a + b;
    }
    @Override public int sub(int a, int b){
    	return a - b;
    }
    @Override public int mul(int a, int b){
    	return a * b;
    }
}
