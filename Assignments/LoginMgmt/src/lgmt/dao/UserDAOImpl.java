package lgmt.dao;
import lgmt.model.User;
import java.util.Scanner;
import java.util.Arrays;

public class UserDAOImpl {

	private User[] users;
	public static Scanner sc = new Scanner(System.in);

    public User[] getUsers(){return users;}

    public void setUsers(User[] users){this.users = users;}

	public void Register(){
		String fname, lname, email, username, password;

		System.out.print("Given name: ");
		fname = sc.next();
		System.out.print("Surname: ");
		lname = sc.next();
		System.out.print("eMail : ");
		email = sc.next();
		System.out.print("User ID: ");
		username = sc.next();
		System.out.print("Password: ");
		password = sc.next();

		User[] userArr = this.getUsers();
		User newUser = new User(fname, lname, email, username, password);
		if (userArr != null){
			userArr = Arrays.copyOf(userArr, userArr.length + 1);
			userArr[userArr.length - 1] = newUser;
			this.setUsers(userArr);
		}else {
			User[] userTemp = new User[1];
			userTemp[0] = newUser;
			this.setUsers(userTemp);
		}

		System.out.println("User Added!");
	}

	public boolean verifyUser(String uname, String password){
		System.out.println(uname + "\t" + password);
		users = this.getUsers();
		for (User i : users){
			if ((i.getUsrn().equals(uname)) && (i.getPwd().equals(password))){
				System.out.println("User found!");
				return true;
			}
		}
		System.out.println("User not found!");
		return false;
	}

	public void test(){System.out.println("Daotest!");}
}
