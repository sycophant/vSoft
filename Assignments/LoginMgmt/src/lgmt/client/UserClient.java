package lgmt.client;
import lgmt.dao.UserDAOImpl;
import java.util.Scanner;

public class UserClient {

	public static Scanner sc = new Scanner(System.in);

	public static void bannerHead(){
		System.out.println("Hello! Register(1) or Login(2) or Exit(3)?");
		System.out.print("\n");
	}

	public static String[] getInfo(){
		String[] userData = new String[2];

    	System.out.println("Username: ");
    	userData[0] = sc.next();
    	System.out.println("Password: ");
    	userData[1] = sc.next();

    	return userData;
	}

    public static void main(String[] args) {
    	UserDAOImpl d = new UserDAOImpl();

    	while(true){
    		bannerHead();
    		int option = sc.nextInt();
    		switch(option){
    			case 1:
    				d.Register();
    				break;
    			case 2:
    				String[] userData = getInfo();
    				System.out.println("Main" + userData[0] + " " + userData[1]);
    				d.verifyUser(userData[0], userData[1]);
    				break;
    			case 3:
    				System.out.println("See ya");
    				System.exit(0);
    				break;
    			default:
    				System.out.println("Please enter an option!");
    		};
    	}
    }
}