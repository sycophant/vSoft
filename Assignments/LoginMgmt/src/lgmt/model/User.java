package lgmt.model;

public class User {

    public User(){
    	System.out.println("No data passed!");
    }

	public User(String fname, String lname, String email, String username, String password){
		this.fname = fname;
		this.lname = lname;
		this.email = email;
		this.username = username;
		this.password = password;
	}

    public String getFName(){return this.fname;}
    public String getLName(){return this.lname;}
    public String getEmail(){return this.email;}
    public String getUsrn(){return this.username;}
    public String getPwd(){return this.password;}

    public void setFName(String fname){this.fname = fname;}
    public void setLName(String lname){this.lname = lname;}
    public void setEmail(String email){this.email = email;}
    public void setUsrn(String username){this.username = username;}
    public void setPwd(String password){this.password = password;}

    public void test(){System.out.println("Usertest!");}

	private String fname, lname, email, username, password;
}