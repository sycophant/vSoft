package com.ems.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.ems.dao.EmployeeDAO;
import com.ems.pojo.Employee;

public class EmployeeDAOImpl implements EmployeeDAO {

	Scanner sc = new Scanner(System.in);
	List<Employee> addEmployees = new ArrayList<>();

	@Override
	public void addEmployee() {
		// TODO Auto-generated method stub
		int k = 1;
		while (k == 1) {
			System.out.println("Enter Employee Number ");
			int eno = sc.nextInt();
			System.out.println("Enter Employee Name");
			String ename = sc.next();
			System.out.println("Enter Employee Address");
			String eadd = sc.next();
			Employee employee = new Employee(eno, ename, eadd);
			addEmployees.add(employee);
			System.out.println("Employee Registred Successfully");
			System.out.println("Do You Want to One more Record 1)Yes 2)No");
			k = sc.nextInt();
		} // end of while

	}

	@Override
	public List<Employee> viewEmployees() {
		// TODO Auto-generated method stub
		return addEmployees;
	}

	@Override
	public Employee viewEmployee(int eno) {
		// TODO Auto-generated method stub
		for (Employee emp : addEmployees) {
			if (emp.getEno() == eno) {
				return emp;
			}
		}

		return null;
	}

	@Override
	public void updateEmployee(int eno) {
		// TODO Auto-generated method stub
		int j = 0;
		for (Employee emp : addEmployees) {
			if (emp.getEno() == eno) {
				++j;
				System.out.println("Do You Want Update 1)Employee Name 2)Employee Address");
				int choice = sc.nextInt();
				switch (choice) {
				case 1:
					System.out.println("Enter Employee Name");
					emp.setEname(sc.next());//
					System.out.println("Employee Name is updated");
					break;
				case 2:
					System.out.println("Enter Employee Address");
					emp.setEadd(sc.next());
					System.out.println("Employee Address is updated");
					break;

				default:

					System.out.println("Choose 1 to 2 Between");

				}// end of switch

			} // end of if

		} // end of for

		if (j == 0)
			System.out.println("Employee Record Doest Not Exist");
	}

	@Override
	public void deleteEmployee(int eno) {
		// TODO Auto-generated method stub

		int j = 0;

		for (Employee emp : addEmployees) {
			if (emp.getEno() == eno) {
				++j;
				addEmployees.remove(emp);
System.out.println("Employee Record Deleted SuccessFully");
				break;
			}
		}

		if (j == 0)
			System.out.println("Employee Record Doest Not Exist");

	}

}
