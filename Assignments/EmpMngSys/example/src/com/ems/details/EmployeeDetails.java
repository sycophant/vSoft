package com.ems.details;

import java.util.List;
import java.util.Scanner;

import com.ems.client.EmployeeClient;
import com.ems.dao.impl.EmployeeDAOImpl;
import com.ems.pojo.Employee;

public class EmployeeDetails {
	Scanner sc = new Scanner(System.in);
	EmployeeDAOImpl daoImpl = new EmployeeDAOImpl();

	public void empDetails() {
		while (true) {
			System.out.println("****************************************************");
			System.out.println("             1)AddEmployee                          ");
			System.out.println("             2)ViewAllEmployees                     ");
			System.out.println("             3)ViewEmployee                     ");
			System.out.println("             4)UpdateEmployees                     ");
			System.out.println("             5)DeleteEmployees                     ");
			System.out.println("             6)Back                     ");
			System.out.println("****************************************************");
			System.out.println("Enter The Choice");
			int choice = sc.nextInt();
			switch (choice) {
			case 1:
				daoImpl.addEmployee();
				empDetails();
				break;
			case 2:
				List<Employee> viewEmployees = daoImpl.viewEmployees();
				System.out.println("*****************************************");
				System.out.println("ENO\t ENAME\t EADD");
				System.out.println("*****************************************");
				for (Employee emp : viewEmployees) {
					System.out.println(emp.getEno() + "\t" + emp.getEname() + "\t" + emp.getEadd());
				}
				empDetails();
				break;
			case 3:
				System.out.println("Enter Employee Number ");
				Employee emp = daoImpl.viewEmployee(sc.nextInt());
				if (emp != null)
					System.out.println(emp.getEno() + "\t" + emp.getEname() + "\t" + emp.getEadd());
				else
					System.out.println("Employee Number Doest Not Exist");
				empDetails();

				break;
			case 4:
				System.out.println("Enter Employee Number ");

				daoImpl.updateEmployee(sc.nextInt());
				empDetails();

				break;
			case 5:

				System.out.println("Enter Employee Number ");
				daoImpl.deleteEmployee(sc.nextInt());
				empDetails();
				break;
			case 6:
				EmployeeClient.main(null);
				break;
			default:
				System.out.println("Choose 1 to 7 between");

			}

		} // end of while
	}

}
