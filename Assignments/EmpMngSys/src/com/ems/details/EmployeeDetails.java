package com.ems.details;

import java.util.Scanner;

import com.ems.dao.impl.EmployeeDAOImpl;

public class EmployeeDetails  {

    public static final String employeeDets = "\n 1 | Add Employee" +
    	"\n 2 | View All Employees" + "\n 3 | View Employee" +
    	"\n 4 | Update Employee" + "\n 5 | Delete Employee" +
    	"\n 6 | Back \n", enoMsg = "\n Provide employee number! \n",
        errorMsg = "\n Please input a valid option! \n";

    public static Scanner scn = new Scanner(System.in);

    public void stringDriver(){

        EmployeeDAOImpl empDao = new EmployeeDAOImpl();

        int driver = 1;

        while(driver == 1){
            
            System.out.println(employeeDets);
            System.out.print(">>\t");

            int options = scn.nextInt();

            switch(options){
                case 1:
                    empDao.addEmployee();
                    break;
                case 2:
                    System.out.println(empDao.viewAllEmployees());
                    break;
                case 3:
                    System.out.println(enoMsg);
                    int eno = scn.nextInt();
                    System.out.println(empDao.viewEmployee(eno));
                    break;
                case 4:
                    System.out.println(enoMsg);
                    int eno2 = scn.nextInt();
                    empDao.updateEmployee(eno2);
                    break;
                case 5:
                    System.out.println(enoMsg);
                    int eno3 = scn.nextInt();
                    empDao.deleteEmployee(eno3);
                    break;
                case 6:
                    driver = 2;
                    break;
                default:
                    System.out.println(errorMsg);
            }
        }
    }
}
