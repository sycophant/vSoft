package com.ems.client;

import java.util.Scanner;

import com.ems.details.EmployeeDetails;

public class EmployeeClient {

	public static final Scanner scn = new Scanner(System.in);

    public static final String
        portalDets = "\n 1 | Employee" + "\n 2 | Exit",
        welcomeMsg = "\n Welcome to the EMS!! \n",
    	errorMsg = "\n Please input a valid option! \n";

    public static void main(String[] args) {

    	EmployeeDetails empDet = new EmployeeDetails();

    	while(true){

    		System.out.println(welcomeMsg + portalDets);
    		System.out.print(">>\t");

    		int options = scn.nextInt();
    		switch(options){
    			case 1:
    				empDet.stringDriver();
    				break;
    			case 2:
    				System.exit(0);
    				break;
    			default:
    				System.out.println(errorMsg);
    		}
    	}
    }
}
