package com.ems.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.naming.NameNotFoundException;

import com.ems.dao.EmployeeDAO;
import com.ems.pojo.Employee;

public class EmployeeDAOImpl implements EmployeeDAO{

	//data structures, objects and string constants
	private static boolean DEBUG_FLAG = false;
	private List<Employee> employees = new ArrayList<Employee>();
	public Scanner scn = new Scanner(System.in);
	public static final String
		ERROR_MSG = "\n Error, employee not available!! \n",
		EMPLOYEE_NUMBER = "\n Please pass employee number. \n",
		EMPLOYEE_NAME = "\n Please pass employee name. \n",
		EMPLOYEE_ADDR = "\n Please pass employee address. \n",
		EMPLOYEE_MOBILE = "\n Please pass employee phone. \n",
		EMPLOYEE_EMAIL = "\n Please pass employee email. \n",
		SUCCESS_STRING = "\n Operation successful!! \n";

	//getter for all the stored employees
	//args: none
	//return: ArrayList<Employee>
	public List<Employee> getEmployees() {
		return employees;
	}

	//setter for all the stored employees
	//args: ArrayList<Employee>
	//return: none
	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	//Helper method that uses scanner class to generate
	//employee object.
	//args: none
	//return: Employee
	public Employee addEmployeeHelper(){

		//scanner block for grabbing data from user
		//Needs data verification.
		System.out.println(EMPLOYEE_NUMBER);
		int eno = scn.nextInt();
		System.out.println(EMPLOYEE_NAME);
		String ename = scn.next();
		System.out.println(EMPLOYEE_ADDR);
		String eaddress = scn.next();
		System.out.println(EMPLOYEE_MOBILE);
		String mobileNumber = scn.next();
		System.out.println(EMPLOYEE_EMAIL);
		String email = scn.next();

		//Attempt to make new object. Prints SUCCESS_STRING
		//or toStrings exception depending on result.
		try{
			var newEmployee = new Employee(eno,
				ename, eaddress, mobileNumber, email);
			System.out.println(SUCCESS_STRING);
			return newEmployee;
		} catch(Exception e){
			if(DEBUG_FLAG == true) e.printStackTrace();
			System.out.println(ERROR_MSG);
			return null;
		}
	}

	//Helper method that uses scanner class to generate
	//employee object.
	//args: none
	//return: Employee
	@Override
	public void addEmployee(){

		var emp = this.addEmployeeHelper();
		var currList = this.getEmployees();

		currList.add(emp);
		this.setEmployees(currList);
	}

	//Calls the getter for all employees.
	//args: none
	//return: ArrayList<Employee>
	@Override
	public List<Employee> viewAllEmployees(){
		return this.getEmployees();
	}

	//Iterates through employee list to find
	//employee based on employee number
	//args: int employeeNumber
	//return: Employee
	@Override
	public Employee viewEmployee(final int eno){
		try{
			Employee emp = null;
			var currList = this.getEmployees();

			//iterate through employees
			for (var i : currList){
				if(i.getEno() == eno){
					emp = i;
					break;
				}
			}

			//throw exception if not found, else print success
			if (emp == null)
				throw new ClassNotFoundException(ERROR_MSG);
			else System.out.println(SUCCESS_STRING);

			return emp;

		} catch(Exception e){
			if(DEBUG_FLAG == true) e.printStackTrace();
			return null;
		}
	}

	//Iterates through employee list to find
	//employee based on employee number
	//args: int employeeNumber
	//return: Employee
	@Override
	public void updateEmployee(final int eno){

		var currEmp = this.viewEmployee(eno);
		var updateEmp = this.addEmployeeHelper();
		var currList = this.getEmployees();

		//use set and indexOf methods to find and set
		//new employee in employee ArrayList.
		try{
			currList.set(currList.indexOf(currEmp), updateEmp);
			this.setEmployees(currList);

		} catch(Exception e){
			if(DEBUG_FLAG == true) e.printStackTrace();
			System.out.println(ERROR_MSG);
		}
	}

	//Iterates through employee list to find employee
	//based on employee number and drop it with remove
	//args: int employeeNumber
	//return: none
	@Override
	public void deleteEmployee(final int eno){
		var currList = this.getEmployees();
		boolean employeeFound = false;

		try{
			for (var i : currList){
				if (i.getEno() == eno){
					currList.remove(i);
					employeeFound = true;
					break;
				}
			}

			if (employeeFound != true) throw new NameNotFoundException();
			
			this.setEmployees(currList);
			System.out.println(SUCCESS_STRING);
			
		} catch(Exception e){
			if(DEBUG_FLAG == true) e.printStackTrace();
			System.out.println(ERROR_MSG);
		}
	}
    
}
