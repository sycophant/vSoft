package com.ems.pojo;

public class Employee {
	private int eno;
    private String ename, eaddress, mobileNumber, email;

    @Override
    public String toString(){
    	String x = this.getEno() + "\n" + this.getEname() + "\n" +
    		this.getEaddress() + "\n" + this.getMobileNumber();

    	return x;
    }

    public Employee(){
    	System.out.println("Employee Created!!");
    }

    public Employee(int eno, String ename, String eaddress,
    		String mobileNumber, String email){
    	this.eno = eno;
    	this.ename = ename;
    	this.eaddress = eaddress;
    	this.mobileNumber = mobileNumber;
    	this.email = email;
    }

	public int getEno() {
		return eno;
	}

	public void setEno(final int eno) {
		this.eno = eno;
	}

	public String getEname() {
		return ename;
	}

	public void setEname(final String ename) {
		this.ename = ename;
	}

	public String getEaddress() {
		return eaddress;
	}

	public void setEaddress(final String eaddress) {
		this.eaddress = eaddress;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(final String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}
}
