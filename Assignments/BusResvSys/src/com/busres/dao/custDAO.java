package com.busres.dao;

public interface custDAO {
    void bookTicket(int ticketID, String startLoc, String endLoc);
    void cancelTicket(int ticketID);
}