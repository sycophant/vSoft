package com.busres.dao;

import java.util.List;

import com.busres.pojo.Bus;

public interface adminDAO {
    void addBus(Bus newBus);
    List<Bus> viewAllBuses();
    Bus viewBus(int busID);
    void updateBus(int busID, Bus tempBus);
    void deleteBus(int busID);
}
