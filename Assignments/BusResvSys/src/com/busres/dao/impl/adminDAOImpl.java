package com.busres.dao.impl;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import com.busres.dao.adminDAO;
import com.busres.pojo.Bus;

public class adminDAOImpl implements adminDAO{

	public List<Bus> getBusList() {
		return busList;
	}
	public void setBusList(List<Bus> busList) {
		this.busList = busList;
	}

    @Override
	public void addBus(Bus newBus){
        try{
            boolean busFound = false;
            this.busList.add(newBus);


            for (var i : this.viewAllBuses()){
                if (i == newBus) busFound = true;
            }

            if (busFound == false) throw new FileNotFoundException();
        }
        catch(FileNotFoundException e){
            if (DEBUG_FLAG == true) e.printStackTrace();
            System.out.println(USER_NOT_FOUND_STRING);
        }
        
	}

    @Override
    public List<Bus> viewAllBuses(){
        return this.busList;
    }

    @Override
    public Bus viewBus(int busID){

        Bus foundBus = null;

    	try{
            for (var i : this.getBusList()){
                if (i.getBusID() == busID){
                    foundBus = i;
                }
            }

            if (foundBus == null) throw new FileNotFoundException();
        }
        catch(FileNotFoundException e){
            if (DEBUG_FLAG == true) e.printStackTrace();
            System.out.println(BUS_NOT_FOUND_STRING);
        }

        return foundBus;
    }

    @Override
    public void updateBus(int busID, Bus newBus){
        try{
            var currList = this.getBusList();
            boolean busFound = false;
    
            for (var i : currList){
                if (i.getBusID() == busID){
                    busFound = true;
                    currList.set(currList.indexOf(i), newBus);
                }
            }
    
            if (busFound == false) throw new FileNotFoundException();

            this.setBusList(currList);
            System.out.println(BUS_FOUND_STRING);
        }
        catch(FileNotFoundException e){
            if(DEBUG_FLAG == true) e.printStackTrace();
            System.out.println(BUS_NOT_FOUND_STRING);
        }
    }

    @Override
    public void deleteBus(int busID){
        try{
            var currList = this.getBusList();
            boolean busFound = false;
            ListIterator<Bus> it = currList.listIterator();

            while(it.hasNext()){
                if (it.next().getBusID() == busID){
                    busFound = true;
                    it.remove();                
                }
            }
    
            if (busFound == false) throw new FileNotFoundException();

            this.setBusList(currList);
            System.out.println(BUS_FOUND_STRING);
        }
        catch(FileNotFoundException e){
            if(DEBUG_FLAG == true) e.printStackTrace();
            System.out.println(BUS_NOT_FOUND_STRING);
        }        
    }

    private static boolean DEBUG_FLAG = true;
    public List<Bus> busList = new ArrayList<Bus>();
    public static final String
        BUS_FOUND_STRING = "\tBus Found!!\t",
        USER_NOT_FOUND_STRING = "\tUser not found!!\t",
        BUS_NOT_FOUND_STRING = "\t Bus not found!! \t";

}
