package com.busres.dao.impl;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.busres.dao.custDAO;
import com.busres.pojo.Ticket;

public class custDAOImpl implements custDAO{

	public List<Ticket> getTicketList() {
		return ticketList;
	}
	public void setTicketList(List<Ticket> ticketList) {
		this.ticketList = ticketList;
	}

    @Override
    public void bookTicket(int ticketID, String startLoc, String endLoc){

        Ticket tick = new Ticket(ticketID, startLoc, endLoc);
        ticketList.add(tick);

        try{
            boolean ticketFlag = false;

            for (var i : this.getTicketList()){
                if (i == tick) ticketFlag = true;
                break;
            }

            if (ticketFlag == false) throw new FileNotFoundException();
        }
        catch(FileNotFoundException e){
            if (DEBUG_FLAG == true) e.printStackTrace();
            System.out.println(TICKET_NOT_FOUND_STRING);
        }

    }

    @Override
    public void cancelTicket(int ticketID){

        try{
            boolean ticketFlag = false;
            List<Ticket> currList = this.getTicketList();

            for (var i : currList){
                if (i.getTicketID() == ticketID){
                    currList.remove(i);
                    ticketFlag = true;
                    break;
                }
            }

            if (ticketFlag == false) throw new FileNotFoundException();
        }
        catch(FileNotFoundException e){
            if (DEBUG_FLAG == true) e.printStackTrace();
            System.out.println(TICKET_NOT_FOUND_STRING);
        }        
    }

    private static boolean DEBUG_FLAG = true;
    public static Scanner scn = new Scanner(System.in);
    public List<Ticket> ticketList = new ArrayList<Ticket>();
    public static final String
        TICKET_NOT_FOUND_STRING = "\tTicket not found!!\t";

}
