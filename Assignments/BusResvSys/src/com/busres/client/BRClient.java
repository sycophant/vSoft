package com.busres.client;

import java.util.Scanner;

import com.busres.details.adminBusDets;
import com.busres.details.custBusDets;

public class BRClient {

	public static final Scanner scn = new Scanner(System.in);

    public static final String
        portalDets = "\n 1 | Admin" + "\n 2 | Customer" + "\n 3 | Exit",
        welcomeMsg = "\n Welcome to the Bus Reservation System!! \n",
    	errorMsg = "\n Please input a valid option! \n";

    public static void main(String[] args) {

    	adminBusDets aDets = new adminBusDets();
        custBusDets cDets = new custBusDets();

    	while(true){

    		System.out.println(welcomeMsg + portalDets);
    		System.out.print(">>\t");

    		int options = scn.nextInt();
    		switch(options){
    			case 1:
    				aDets.stringDriver();
    				break;
    			case 2:
    				cDets.stringDriver();
    				break;
    			case 3:
    				System.exit(0);
                    break;
    			default:
    				System.out.println(errorMsg);
    		}
    	}
    }
}
