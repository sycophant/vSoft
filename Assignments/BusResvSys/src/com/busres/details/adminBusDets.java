package com.busres.details;

import java.util.Scanner;

import com.busres.dao.impl.adminDAOImpl;
import com.busres.pojo.Bus;

public class adminBusDets {
    public static final String BUS_DETS = "\n 1 | Add Bus" +
    	"\n 2 | View All Buses" + "\n 3 | View Bus" +
    	"\n 4 | Update Bus" + "\n 5 | Delete Bus" +
    	"\n 6 | Back \n",
        BUS_NO = "\n Provide Bus number! \n",
        BUS_NAME = "Provide bus name!!",
        ERR_MSG = "\n Please input a valid option! \n";

    public static Scanner scn = new Scanner(System.in);

    public static Bus busHelper(){
        System.out.println(BUS_NO);
        int busID = scn.nextInt();
        System.out.println(BUS_NAME);
        String busName = scn.next();

        Bus newBus = new Bus(busID, busName);

        return newBus;
    }

    public void stringDriver(){

        adminDAOImpl aImpl = new adminDAOImpl();

        int driver = 1;

        while(driver == 1){
            
            System.out.println(BUS_DETS);
            System.out.print(">>\t");

            int options = scn.nextInt();

            switch(options){
                case 1:
                    var busTemp = busHelper();
                    aImpl.addBus(busTemp);
                    break;
                case 2:
                    System.out.println(aImpl.viewAllBuses());
                    break;
                case 3:
                    System.out.println(BUS_NO);
                    int eno = scn.nextInt();
                    System.out.println(aImpl.viewBus(eno));
                    break;
                case 4:
                    System.out.println(BUS_NO);
                    int eno2 = scn.nextInt();
                    var busTemp2 = busHelper();
                    aImpl.updateBus(eno2, busTemp2);
                    break;
                case 5:
                    System.out.println(BUS_NO);
                    int eno3 = scn.nextInt();
                    aImpl.deleteBus(eno3);
                    break;
                case 6:
                    driver = 2;
                    break;
                default:
                    System.out.println(ERR_MSG);
            }
        }
    }
}
