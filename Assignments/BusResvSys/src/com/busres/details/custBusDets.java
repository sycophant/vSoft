package com.busres.details;

import java.util.Scanner;

import com.busres.dao.impl.adminDAOImpl;
import com.busres.dao.impl.custDAOImpl;

public class custBusDets {
    public static final String employeeDets =
    	"\n 1 | View All Buses" + "\n 2 | View Bus" +
    	"\n 3 | Book Ticket" + "\n 4 | Cancel Ticket" +
    	"\n 5 | Back \n", enoMsg = "\n Provide bus number! \n",
        eLocMsg = "\n Provide start and end location!! \n",
        errorMsg = "\n Please input a valid option! \n";

    public static Scanner scn = new Scanner(System.in);

    public void stringDriver(){
        adminDAOImpl aImpl = new adminDAOImpl();
        custDAOImpl cImpl = new custDAOImpl();

        int driver = 1;

        while(driver == 1){
            
            System.out.println(employeeDets);
            System.out.print(">>\t");

            int options = scn.nextInt();

            switch(options){
                case 1:
                    System.out.println(aImpl.viewAllBuses());
                    break;
                case 2:
                    int eno1 = scn.nextInt();
                    System.out.println(aImpl.viewBus(eno1));
                    break;
                case 3:
                    System.out.println(enoMsg);
                    int eno2 = scn.nextInt();
                    System.out.println(eLocMsg);
                    String eStart = scn.next(), eEnd = scn.next();
                    cImpl.bookTicket(eno2, eStart, eEnd);
                    break;
                case 4:
                    System.out.println(enoMsg);
                    int eno3 = scn.nextInt();
                    cImpl.cancelTicket(eno3);;
                    break;
                case 5:
                    driver = 2;
                    break;
                default:
                    System.out.println(errorMsg);
            }
        }
    }
}
