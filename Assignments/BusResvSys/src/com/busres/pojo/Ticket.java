package com.busres.pojo;

public class Ticket {

	public int getTicketID() {
		return ticketID;
	}
	public void setTicketID(int ticketID) {
		this.ticketID = ticketID;
	}
	public String getStartLoc() {
		return startLoc;
	}
	public void setStartLoc(String startLoc) {
		this.startLoc = startLoc;
	}
	public String getEndLoc() {
		return endLoc;
	}
	public void setEndLoc(String endLoc) {
		this.endLoc = endLoc;
	}

	public Ticket(int ticketID, String startLoc, String endLoc){
		this.startLoc = startLoc;
		this.endLoc = endLoc;
		this.ticketID = ticketID;
	}

	public Ticket(){
		System.out.println("\tTicket is made!!\t");
	}

	@Override
	public String toString() {
	    return this.getTicketID() + "\n" +
	    this.getClass() + "\t" +
	    this.getEndLoc();
	}

	private String startLoc, endLoc;
	private int ticketID;
}