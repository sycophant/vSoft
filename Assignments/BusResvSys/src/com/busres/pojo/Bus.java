package com.busres.pojo;

public class Bus {

	public int getBusID() {
		return busID;
	}
	public void setBusID(int busID) {
		this.busID = busID;
	}
	public String getBusName() {
		return busName;
	}
	public void setBusName(String busName) {
		this.busName = busName;
	}

    @Override
    public String toString() {
        return this.hashCode() + "\n" +
        this.busName + "\t" +
        this.busID + "\t";
    }

    public Bus(int busID, String busName){
        this.busID = busID;
        this.busName = busName;
    }

    Bus(){
        System.out.println("New bus!!\t" + this.hashCode());
    }

    private int busID;
    private String busName;
}
