package com.Employee.demo.repository;

import com.Employee.demo.entity.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddrRep extends JpaRepository<Address, Integer> {
	public Address findAddrByApt(int apt);
}
