package com.Employee.demo.service;

import com.Employee.demo.entity.Address;
import com.Employee.demo.entity.PartTimer;
import com.Employee.demo.repository.AddrRep;
import com.Employee.demo.repository.PartRep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service public class PartTServ {

	@Autowired @Lazy private PartRep partRep;
	@Autowired @Lazy private AddrRep addrRep;

	public List<PartTimer> GetPartTimers() {
		List<PartTimer> lstPart = partRep.findAll();
		if (!lstPart.isEmpty()) return lstPart;
		else return null;
	}

	public void AddPartTimer(PartTimer partTimer) {
		if (partTimer.getAddr() != null) {
			Address addr = addrRep.findAddrByApt(
					(partTimer.getAddr()).getApt());
			if (addr != null) partTimer.setAddr(addr);
		}
		partRep.save(partTimer);
	}
}
