package com.Employee.demo.repository;

import com.Employee.demo.entity.PartTimer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PartRep extends JpaRepository<PartTimer, Integer> {
}
