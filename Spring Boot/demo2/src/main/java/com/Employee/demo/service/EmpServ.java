package com.Employee.demo.service;

import com.Employee.demo.entity.Employee;
import com.Employee.demo.repository.EmpRep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.List;
@Service public class EmpServ {

	@Autowired @Lazy private EmpRep empRep;

	public List<Employee> GetEmployees() {return empRep.findAll();}

	public void AddEmployee(Employee employee) {empRep.save(employee);}
}
