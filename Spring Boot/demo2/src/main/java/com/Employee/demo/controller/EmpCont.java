package com.Employee.demo.controller;

import com.Employee.demo.entity.Employee;
import com.Employee.demo.repository.AddrRep;
import com.Employee.demo.service.EmpServ;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController @RequestMapping("api/employees")
public class EmpCont {

	@Autowired @Lazy private EmpServ empServ;
	@Autowired @Lazy private AddrRep addrRep;

	// Using ResponseEntity to handle status codes
	// returns Data and 200 when found
	// returns 404 and String when not found
	@GetMapping public List<Employee> GetEmployees(){
		return empServ.GetEmployees();
	}

	@PostMapping @ResponseBody public void AddEmployee(
			@RequestBody Employee employee){
		empServ.AddEmployee(employee);
	}
}
