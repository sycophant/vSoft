package com.Employee.demo.repository;

import com.Employee.demo.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmpRep extends JpaRepository<Employee, Integer> {
}
