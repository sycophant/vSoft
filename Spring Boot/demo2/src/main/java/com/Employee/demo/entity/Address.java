package com.Employee.demo.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity @Getter @Setter @Table(name = "addresses")
@NoArgsConstructor @AllArgsConstructor
public class Address {
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer addr_id;
	private Integer apt, post;
	private String loc;

	@OneToMany(mappedBy = "addr", cascade = CascadeType.ALL)
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	List<Employee> employeeList;
	@OneToMany(mappedBy = "addr", cascade = CascadeType.ALL)
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	List<PartTimer> partTimerList;
}
