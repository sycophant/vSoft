package com.Employee.demo.entity;

import lombok.*;

import javax.persistence.*;

@Entity @Getter @Setter @ToString @Table(name = "employees")
@NoArgsConstructor @AllArgsConstructor
@RequiredArgsConstructor
public class Employee {
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer emp_id;
	@NonNull private String name, email;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "addr_id")
	Address addr;
}
