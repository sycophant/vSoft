package com.Employee.demo.controller;

import com.Employee.demo.entity.PartTimer;
import com.Employee.demo.service.PartTServ;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController @RequestMapping("api/part_timer")
public class PartTCont {

	@Autowired @Lazy private PartTServ partTServ;

	@GetMapping public ResponseEntity<List<PartTimer>> GetPartTimers(){
		List<PartTimer> lstPT = partTServ.GetPartTimers();
		if (lstPT == null) return
				new ResponseEntity<>(HttpStatus.NOT_FOUND);
		else return
				new ResponseEntity<>(lstPT, HttpStatus.FOUND);
	}

	@PostMapping public void AddPartTimer(@RequestBody PartTimer partTimer){
		partTServ.AddPartTimer(partTimer);
	}
}
