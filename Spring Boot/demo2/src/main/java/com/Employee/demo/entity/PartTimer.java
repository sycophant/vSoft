package com.Employee.demo.entity;

import lombok.*;

import javax.persistence.*;

@Entity @Getter @Setter @ToString @Table(name = "part_timers")
@NoArgsConstructor @AllArgsConstructor
@RequiredArgsConstructor
public class PartTimer {
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer part_id;
	@NonNull private String name, email;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "addr_id")
	Address addr;
}
