package com.example.springboot.springboot.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.example.springboot.springboot.model.User;
import com.example.springboot.springboot.web.dto.UserRegistrationDto;

public interface UserService extends UserDetailsService{
	User save(UserRegistrationDto registrationDto);
}
