package com.SpringTest.demo.configuration;

import java.time.LocalDate;
import java.util.List;

import com.SpringTest.demo.pojo.Student;
import com.SpringTest.demo.repository.StudentRepository;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class StudentConfig {

	@Bean
	CommandLineRunner commandLineRunner(StudentRepository StuRep){
		return args -> {
			Student a = new Student(
					"Weed",
					"Weed@dude.lmao",
					LocalDate.of(2000, 1, 25)
			);
			Student b = new Student(
					"Haha",
					"lmao@dude.weed",
					LocalDate.of(2000, 2, 10)
			);

			StuRep.saveAll(
					List.of(a, b)
			);
		};
	}

}
