package com.SpringTest.demo.controllers;

import java.util.List;

import com.SpringTest.demo.pojo.Student;
import com.SpringTest.demo.services.StudentService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.*;

@RestController @RequestMapping(path = "api/student")
public class StudentController {

	private final StudentService stuServ;

	@Autowired @Lazy
	public StudentController(StudentService stuServ) {
		this.stuServ = stuServ;
	}

	@GetMapping
	public List<Student> getStudents(){
		return stuServ.getStudents();
	}

	@PostMapping
	public void addStudent(@RequestBody Student stu){
		stuServ.addStudent(stu);
	}

	@DeleteMapping(path = "{stuID}")
	public void deleteStudent(@PathVariable("stuID") Long studentID){
		stuServ.deleteStudent(studentID);
	}
}
