package com.SpringTest.demo.services;

import java.util.List;
import java.util.Optional;

import com.SpringTest.demo.pojo.Student;
import com.SpringTest.demo.repository.StudentRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

@Service
public class StudentService {

	private final StudentRepository stuRep;

	@Autowired @Lazy
	public StudentService(StudentRepository stuRep) {
		this.stuRep = stuRep;
	}

	public List<Student> getStudents(){
		return stuRep.findAll();
	}

	public void addStudent(Student stu) {
		Optional<Student> studentOpt = stuRep
				.findByEmailEquals(stu.getEmail());
		if (studentOpt.isPresent()) {
			throw new IllegalStateException("Email is taken!!");
		} else stuRep.save(stu);
	}

	public void deleteStudent(Long stuID) {
		boolean exists = stuRep.existsById(stuID);

		if (!exists){
			throw new IllegalStateException(
					"Student" + stuID + "does not exist!!"
			);
		} else stuRep.deleteById(stuID);
	}
}
