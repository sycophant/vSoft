package com.SpringTest.demo.pojo;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.Period;

@Entity @Table @Getter @Setter @ToString
@NoArgsConstructor @AllArgsConstructor
@RequiredArgsConstructor
public class Student {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long sid;
	@NonNull private String name;
	@NonNull private String email;
	@NonNull private LocalDate dob;
	@Transient private Integer age;

	public Integer getAge() {
		return Period
				.between(this.dob, LocalDate.now())
				.getYears();
	}
}
