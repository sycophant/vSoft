package com.SpringTest.demo.repository;

import java.util.Optional;

import com.SpringTest.demo.pojo.Student;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository
		extends JpaRepository<Student, Long> {

	Optional<Student> findByEmailEquals(String email);
}
