package com.SpringTest.demo.pojo;

import java.time.LocalDate;
import javax.annotation.processing.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Student.class)
public abstract class Student_ {

	public static volatile SingularAttribute<Student, String> sname;
	public static volatile SingularAttribute<Student, LocalDate> dob;
	public static volatile SingularAttribute<Student, String> email;
	public static volatile SingularAttribute<Student, Long> sid;

	public static final String SNAME = "sname";
	public static final String DOB = "dob";
	public static final String EMAIL = "email";
	public static final String SID = "sid";

}

