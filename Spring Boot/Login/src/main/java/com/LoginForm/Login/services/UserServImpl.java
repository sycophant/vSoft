package com.LoginForm.Login.services;

import com.LoginForm.Login.entities.User;
import com.LoginForm.Login.repos.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

@Service public class UserServImpl implements UserServ {
	@Autowired private UserRepo userRepo;

	@Override public User UserLogin(User user){
		return userRepo.findByUnameAndPasswordEquals(
				user.getUname(), user.getPassword());
	}

	@Override public boolean UserRegister(User user) {
		if (this.UserLogin(user) == null) {
			userRepo.save(user);
			return true;
		}else return false;
	}
}
