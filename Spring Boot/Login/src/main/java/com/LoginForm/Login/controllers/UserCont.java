package com.LoginForm.Login.controllers;

import com.LoginForm.Login.entities.User;
import com.LoginForm.Login.services.UserServImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserCont {
	@Autowired private UserServImpl userServ;

	@PostMapping(value = "api/login")
	public ResponseEntity<User> UserLogin(@RequestBody User user){
		User foundUser = userServ.UserLogin(user);
		if (foundUser != null)
			return new ResponseEntity<>(foundUser, HttpStatus.ACCEPTED);
		else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@PostMapping(value = "api/register")
	public ResponseEntity<String> UserRegister(@RequestBody User user){
		if (userServ.UserRegister(user))
			return new ResponseEntity<>(HttpStatus.ACCEPTED);
		else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
}
