package com.LoginForm.Login.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PageCont {

	@GetMapping(value = "/")
	public String LoginPortal(){
		return "login.html";
	}

	@GetMapping(value = "/books")
	public String BookPortal(){
		return "books.html";
	}
}
