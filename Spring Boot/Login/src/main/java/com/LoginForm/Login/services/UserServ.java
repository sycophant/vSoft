package com.LoginForm.Login.services;

import com.LoginForm.Login.entities.User;

public interface UserServ {
	User UserLogin(User user);
	boolean UserRegister(User user);
}
