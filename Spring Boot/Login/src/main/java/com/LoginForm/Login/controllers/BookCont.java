package com.LoginForm.Login.controllers;

import com.LoginForm.Login.entities.Book;
import com.LoginForm.Login.services.BookServImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController @RequestMapping(value = "api/books")
public class BookCont {
	@Autowired BookServImpl bookServ;

	@PostMapping
	public ResponseEntity<String> AddBook(@RequestBody Book book){
		if (bookServ.AddBook(book))
			return new ResponseEntity<>(HttpStatus.CREATED);
		else return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
	}

	@PutMapping
	public ResponseEntity<String> EditBook(@RequestBody Book book){
		if (bookServ.EditBook(book))
			return new ResponseEntity<>(HttpStatus.ACCEPTED);
		else return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
	}

	@DeleteMapping
	public ResponseEntity<String> DeleteBook(@RequestParam Long bID){
		if (bookServ.DeleteBook(bID))
			return new ResponseEntity<>(HttpStatus.ACCEPTED);
		else return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
	}

	@GetMapping
	public ResponseEntity<List<Book>> GetBooks(){
		List<Book> bookList = bookServ.GetBooks();
		if (bookList.isEmpty())
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		else return new ResponseEntity<>(bookList, HttpStatus.FOUND);
	}
}
