package com.LoginForm.Login.entities;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity @Table(name = "users") @Getter @Setter @ToString
@NoArgsConstructor @AllArgsConstructor @RequiredArgsConstructor
public class User {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id private Long uid;
	@NonNull private String uname, password;
	private Long phone;
	private String fname, lname, email, address;
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "user_books", joinColumns = {@JoinColumn(
			name = "uname", referencedColumnName = "uname")
	}, inverseJoinColumns = {@JoinColumn(
			name = "bname", referencedColumnName = "bname")
	}) @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	private List<Book> bookList;
}
