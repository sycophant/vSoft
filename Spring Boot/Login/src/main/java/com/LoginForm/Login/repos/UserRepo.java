package com.LoginForm.Login.repos;

import com.LoginForm.Login.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepo extends JpaRepository<User, Long> {
	public User findByUnameAndPasswordEquals(
			String uname, String password);
}