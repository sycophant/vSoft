package com.LoginForm.Login.services;

import com.LoginForm.Login.entities.Book;

import java.util.List;

public interface BookServ {
	boolean AddBook(Book book);
	List<Book> GetBooks();
	boolean EditBook(Book book);
	boolean DeleteBook(Long bID);
}
