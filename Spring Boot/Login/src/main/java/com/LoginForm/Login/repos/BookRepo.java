package com.LoginForm.Login.repos;

import com.LoginForm.Login.entities.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepo extends JpaRepository<Book, Long> {
	boolean existsByBnameAndAname(String bname, String aname);
}
