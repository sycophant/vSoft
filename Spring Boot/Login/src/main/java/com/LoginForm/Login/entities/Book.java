package com.LoginForm.Login.entities;

import lombok.*;

import javax.persistence.*;

@Entity @Table(name = "books") @Getter @Setter @ToString
@NoArgsConstructor @AllArgsConstructor @RequiredArgsConstructor
public class Book {
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long bid;
	@NonNull private String bname, aname;
	@NonNull private Long price;
}
