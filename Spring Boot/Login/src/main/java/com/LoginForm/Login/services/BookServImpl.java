package com.LoginForm.Login.services;

import com.LoginForm.Login.entities.Book;
import com.LoginForm.Login.repos.BookRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServImpl implements BookServ {
	@Autowired BookRepo bookRepo;

	@Override public boolean AddBook(Book book){
		if (bookRepo.existsByBnameAndAname(
				book.getBname(), book.getAname()))
			return false;
		else {
			bookRepo.save(book);
			return true;
		}
	}

	@Override
	public boolean EditBook(Book book) {
		if (bookRepo.existsById(book.getBid())){
			Book oldBook = bookRepo.getById(book.getBid());
			oldBook.setBname(book.getBname());
			oldBook.setAname(book.getAname());
			oldBook.setPrice(book.getPrice());
			bookRepo.save(oldBook);
			return true;
		}else return false;
	}

	@Override public List<Book> GetBooks() {
		return bookRepo.findAll();
	}

	public boolean DeleteBook(Long bID) {
		if (bookRepo.existsById(bID)){
			bookRepo.deleteById(bID);
			return true;
		}else return false;
	}
}
