import java.util.Scanner;

public class CalculateEx {

	int fno;
	int sno;
	int result = 0;
	static Scanner sc = new Scanner(System.in);

	void add() {
		System.out.println("Enter The First Number :");
		fno = sc.nextInt();
		System.out.println("Enter The Second Number :");
		sno = sc.nextInt();
		result = fno + sno;
		System.out.println("Adding of Two Numbers : " + result);
	}

	void sub() {
		System.out.println("Enter The First Number :");
		fno = sc.nextInt();
		System.out.println("Enter The Second Number :");
		sno = sc.nextInt();
		result = fno - sno;
		System.out.println("Subtracting of Two Numbers : " + result);
	}

	void mul() {
		System.out.println("Enter The First Number :");
		fno = sc.nextInt();
		System.out.println("Enter The Second Number :");
		sno = sc.nextInt();
		result = fno * sno;
		System.out.println("Multiply of Two Numbers : " + result);
	}

	void div() {
		System.out.println("Enter The First Number :");
		fno = sc.nextInt();
		System.out.println("Enter The Second Number :");
		sno = sc.nextInt();
		result = fno / sno;
		System.out.println("divison of Two Numbers : " + result);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CalculateEx demo = new CalculateEx();
		while (true) {
			System.out.println("***************************************");
			System.out.println("           1)ADD                       ");
			System.out.println("           2)SUB                       ");
			System.out.println("           3)MUL                       ");
			System.out.println("           4)DIV                       ");
			System.out.println("           5)EXIT                      ");

			System.out.println("***************************************");
			System.out.println("Enter The Choice");
			int choice = sc.nextInt();
			switch (choice) {
			case 1:
				demo.add();

				break;

			case 2:
				demo.sub();

				break;

			case 3:
				demo.mul();

				break;

			case 4:
				demo.div();
				break;

			case 5:

				System.out.println("Thx for Using App!");
				System.exit(0);

			default:
				System.out.println("Choose 1 to 5 Between");

			}// end of switch

		} // end of while
	}

}
