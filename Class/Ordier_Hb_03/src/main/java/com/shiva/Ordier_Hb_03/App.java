package com.shiva.Ordier_Hb_03;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.shiva.pojo.OrderDetails;

/**
 * Hello world!
 *
 */
// create sequence ORDER_ID_SEQ start with 100 increment by 1;
public class App {
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        // Step 1: Create Configuration Object
        Configuration cfg = new Configuration();
        cfg.configure();
        // Step 2: Build Session Factory
        SessionFactory factory = cfg.buildSessionFactory();

        // Step 3: Open Session
        Session ses = factory.openSession();
        Transaction tx = ses.beginTransaction();
        OrderDetails details = new OrderDetails();
        details.setOrderBy("shiva");
        details.setOrderPlaceDate(new Date());
        ses.save(details);
        tx.commit();

    }
}
