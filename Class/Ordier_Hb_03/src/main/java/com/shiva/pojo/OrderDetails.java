package com.shiva.pojo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "ORDER_DTLS")
public class OrderDetails {
	@Id
	@Column(name = "ORDER_ID")
	@GenericGenerator(name = "order_id_gen", strategy = "com.shiva.pojo.OrderIdGenerator")
	@GeneratedValue(generator = "order_id_gen")
	private String orderid;// od1 od2
	@Column(name = "ORDER_BY")
	private String orderBy;
	@Column(name = "ORDER_PLACED_DT")
	private Date orderPlaceDate;

	public String getOrderid() {
		return orderid;
	}

	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public Date getOrderPlaceDate() {
		return orderPlaceDate;
	}

	public void setOrderPlaceDate(Date orderPlaceDate) {
		this.orderPlaceDate = orderPlaceDate;
	}

}
