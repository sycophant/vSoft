package com.shiva.pojo;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

public class OrderIdGenerator implements IdentifierGenerator {

	public Serializable
	generate(SharedSessionContractImplementor session, Object object)
	throws HibernateException {
		// TODO Auto-generated method stub
		Integer seqVal = null;
		String prefix = "OD";
		Connection con = null;
		con = session.connection();
		Statement st;
		try {
			st = con.createStatement();
			ResultSet rs = st.executeQuery("select ORDER_ID_SEQ.NEXTVAL FROM DUAL");
			if (rs.next()) {
				seqVal = rs.getInt(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return prefix + "-" + seqVal;
	}

}
