package com.example;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import com.example.entity.CourseEntity;
import com.example.entity.StudentEntity;
import com.example.repo.CourseRepo;
import com.example.repo.StudentRepo;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		ConfigurableApplicationContext ctx=		SpringApplication.run(Application.class, args);
		
		CourseRepo courseRepo = ctx.getBean(CourseRepo.class);
		StudentRepo stuRepo=ctx.getBean(StudentRepo.class);
		
		
//		  CourseEntity course=new CourseEntity(); 
//		  course.setCname("AngularJs");
//		  course.setCduration("30"); 
//		  course.setCfee(15000); 
//		  CourseEntity savedEntity= courseRepo.save(course); 
//		  if(savedEntity!=null)
//		  System.out.println(savedEntity.getCname()+"\t"+savedEntity.getCduration()+
//		  "\t"+savedEntity.getCfee());
//		 	
//		
//		
		
		
		
		
		
		
		
		
		
//		List<CourseEntity>addCourses=courseRepo.findAll();
//		addCourses.forEach(course->
//		{
//			System.out.println(course.getCname()+"\t"+course.getCduration()+"\t"+course.getCfee());
//		}
//				
//				);
//		
		
		
	
//	  StudentEntity stu=new StudentEntity(); 
//	  stu.setSname("arya");
//	  stu.setSadd("usa"); 
//	  StudentEntity stu1=new StudentEntity();
//	  stu1.setSname("Jasmin");
//	  stu1.setSadd("usa");
//	  
//	  
//	  Set<StudentEntity> courseSet=new HashSet<StudentEntity>();
//	  courseSet.add(stu); 
//	  courseSet.add(stu1);
//	  
//	  CourseEntity course=new CourseEntity(); 
//	  course.setCname("Boomi");
//	  course.setCduration("20"); 
//	  course.setCfee(25000);
//	  course.setStudents(courseSet);
//	  courseRepo.save(course);
//	 
		
		
		  Optional<CourseEntity> findById = courseRepo.findById(1); 
		  CourseEntity
		  courseEntity = findById.get();
		  
		  Set<StudentEntity> students = courseEntity.getStudents();
		  
		  System.out.println("Course Details : ");
		  System.out.println(courseEntity.getCname()+"\t"+courseEntity.getCduration()+
		  "\t"+courseEntity.getCfee());
		  
		  System.out.println("Student Details : "); for(StudentEntity stu : students) {
		  System.out.println(stu.getSname()+"\t"+stu.getSadd()); }
		 
	}

}
