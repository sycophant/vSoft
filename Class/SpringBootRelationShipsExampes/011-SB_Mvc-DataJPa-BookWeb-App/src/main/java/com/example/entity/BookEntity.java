package com.example.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Entity
@Table(name = "BOOK_TBL")
@Data
public class BookEntity {
@Id
@GenericGenerator(name = "myGenerator",strategy = "increment")
@GeneratedValue(generator = "myGenerator",strategy = GenerationType.AUTO)


@Column(name = "BOOK_ID")
private Integer bookId;
@Column(name = "BOOK_NAME")
private String bookName;
@Column(name = "AUTHOR_NAME")
private String authorName;
@Column(name = "BOOK_PRICE")
private String bookPrice;
}
