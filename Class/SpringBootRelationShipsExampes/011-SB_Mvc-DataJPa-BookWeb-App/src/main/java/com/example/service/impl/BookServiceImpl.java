package com.example.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.bindings.Book;
import com.example.entity.BookEntity;
import com.example.repo.BookRepo;
import com.example.service.BookService;

@Service
public class BookServiceImpl implements BookService {

	@Autowired
	private BookRepo bookDao;
	
	@Override
	public boolean saveBook(Book book) {
		// TODO Auto-generated method stub
		//we got data in binding class object to save
		//to save we should call repo.save(T entity) which expects entity obj with data
		BookEntity bentity=new BookEntity();
		System.out.println(book);
		
		//copy data from binding obj to entity object
		BeanUtils.copyProperties(book, bentity);
		
		/*
		 * bentity.setBookName(book.getBookName());
		 * bentity.setAuthorName(book.getAuthorName());
		 * bentity.setBookPrice(book.getBookPrice());
		 */
		BookEntity savedEntity=		bookDao.save(bentity);
		System.out.println(savedEntity);
		return savedEntity.getBookId()!=null;
	}

	@Override
	public List<Book> getAllBooks() {
		// TODO Auto-generated method stub
		
		List<BookEntity> entityList = bookDao.findAll();
		
		List<Book> books=new ArrayList<Book>();
		
		entityList.forEach(entity->
		
		{
			Book b=new Book();
			BeanUtils.copyProperties(entity, b);
			
			books.add(b);
		}
				
				);
		
		
		return books;
	}

	@Override
	public Book getBookById(int id) {
		// TODO Auto-generated method stub
		Optional<BookEntity> findById = bookDao.findById(id);
		BookEntity bentity=	findById.get();
		
		Book book=new Book();
		BeanUtils.copyProperties(bentity, book);
		
		return book;
	}

	@Override
	public boolean updateBook(Book st) {
		// TODO Auto-generated method stub
		System.out.println(st.getBookId()+"service Impl");
		Optional<BookEntity> findById = bookDao.findById(st.getBookId());
		BookEntity bentity=	findById.get();
		
		BeanUtils.copyProperties(st, bentity);
		System.out.println(bentity.getBookId()+"entity");
		
		
		
		
		return bookDao.save(bentity)!=null;
	}

	@Override
	public void deleteBook(int id) {
		// TODO Auto-generated method stub
		bookDao.deleteById(id);
		
		System.out.println("Given Id is Deleted");
		
		
	}

}
