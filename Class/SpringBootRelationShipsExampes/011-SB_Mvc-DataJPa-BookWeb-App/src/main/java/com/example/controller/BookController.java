package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.bindings.Book;
import com.example.service.BookService;

@Controller
public class BookController {
	
	@Autowired
	private BookService bookService;

	@GetMapping(value = {"/","/bookForm"})
	public String loadForm(Model model)
	{
		Book b=new Book();
		model.addAttribute("book",b);
		return "addBook";
	}
	
	@PostMapping(value = "/addBook")
	public String handleaddBookBtn(@ModelAttribute("book")   Book book,Model model)
	{
		
		
		
	boolean isSaved=	bookService.saveBook(book);
		
		String msg="";
		
		if(isSaved)
		{
			msg="Book Added SuccessFully";
		}
		else
		{
			msg="Failed to Add Book";
		}
		model.addAttribute("msg",msg);
		return "addBook";
	}
	
	
	@GetMapping("/viewBooks")
	public String viewAllBooks(Model model)
	{
		List<Book>booksList=bookService.getAllBooks();
		model.addAttribute("books",booksList);
		return "viewBooks";
	}
	
	
	@RequestMapping(value = "/editstu")
	public String edit(@RequestParam int id, Model m) {
		Book book=bookService.getBookById(id);
		
		m.addAttribute("book",book);
		
		return "bookeditform";
	}

	
	 // It updates model object.
	  
	  @PostMapping(value="/editBook")
	  public String	  editsave(@ModelAttribute("book") Book book,Model model) 
	  {
		  System.out.println(book.getBookId()+"controller");
		  boolean  isSaved = bookService.updateBook(book);
		  String msg = "";
	  System.out.println("welcome for updating"+isSaved);
	  if (isSaved) 
	  { msg = "Book Updated Successfully"; } 
	  else { msg =
	  "Failed to Updated Record"; }
	  
	  model.addAttribute("msg", msg);
	  
	  return "redirect:/viewBooks";
	  
	  }
	
	
	
	
	
	
	
	
	
	
	
	
	
	@GetMapping("/deleteBook")
	public String deleteBookId(@RequestParam("id") Integer id,Model model)
	{
		
		bookService.deleteBook(id);
		
		return "redirect:/viewBooks";
	}
	
	
	
	
	
	
}
