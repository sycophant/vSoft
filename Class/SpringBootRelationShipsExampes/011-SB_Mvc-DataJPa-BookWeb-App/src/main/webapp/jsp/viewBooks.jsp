<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script
	src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<!-- //cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css
 --><link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css"
	rel="stylesheet" type="text/css">
<script>
$(document).ready(function() {
	$('#stutab').DataTable({
		"pagingType" : "full_numbers"
	});
});
</script>
<script>
	function deleteConfirm(){
	var c=confirm("Are you sure, you want to delete?")
	console.log(c);
		return c;
	}
</script>

</head>
<body>
<h3>View  Books</h3>
<a href="bookForm">Add New Book</a>
<table id="stutab">
<thead>
<tr>
<th>
Book Id
</th>
<th>
Book Name
</th>
<th>
Author Name
</th>
<th>
Book Price</th>
<th>Edit</th>
<th>Delete</th>
</tr>
</thead>
<tbody>
<c:forEach items="${books}" var="b">
<tr>
<td>${b.bookId}</td>

<td>${b.bookName}</td>
<td>${b.authorName}</td>
<td>${b.bookPrice}</td>
  <td><a href="editstu?id=${b.bookId}">Edit</a></td>
     <td><a href="deleteBook?id=${b.bookId}" onclick="deleteConfirm()">Delete</a>

</tr>
</c:forEach>

</tbody>

</table>
</body>
</html>