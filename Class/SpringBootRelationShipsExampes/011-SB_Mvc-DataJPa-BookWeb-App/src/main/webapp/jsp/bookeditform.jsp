<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<h3>Edit Book</h3>
<form:form action="editBook" modelAttribute="book" method="post">

<table>
<tr>
<form:hidden path="bookId"/>
<td>
Book Name :
</td>
<td>
<form:input path="bookName"/>
</td>

</tr>

<tr>
<td>
Author Name :
</td>
<td>
<form:input path="authorName"/>
</td>

</tr>

<tr>
<td>
Book Price :
</td>
<td>
<form:input path="bookPrice"/>
</td>

</tr>
<tr>
          <td><input type="submit" value="Edit Save" /></td>  

</tr>
</table>






</form:form>

</body>
</html>